/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #3: Smart Dam
   Author:
   - Andrea Casadei 800898
*/

#ifndef ULTRASONICSENSOR_H
#define ULTRASONICSENSOR_H

class UltrasonicSensor
{
  public:
    UltrasonicSensor(unsigned triggerPin, unsigned echoPin);
    double read();

  private:
    const unsigned int firstDelay = 3;
    const unsigned int secondDelay = 5;
    const float soundSpeed = 331.5 + 0.6*20;
    unsigned triggerPin;
    unsigned echoPin;
};

#endif
