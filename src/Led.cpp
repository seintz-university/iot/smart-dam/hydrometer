/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #3: Smart Dam
   Author:
   - Andrea Casadei 800898
*/

#include "Led.h"
#include <Arduino.h>

Led::Led(unsigned pin) {
    this->pin = pin;
    digitalWrite(pin, LOW);
    isOn = false;
}

void Led::switchOn() {
    digitalWrite(pin, HIGH);
    isOn = true;
}

void Led::switchOff() {
    digitalWrite(pin, LOW);
    isOn = false;
}

bool Led::isTurnedOn() {
    return isOn;
}
